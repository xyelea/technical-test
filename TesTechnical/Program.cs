﻿using System;
// Palindrom = kata, frasa, angka, atau urutan karakter lainnya yang memiliki sifat yang sama jika dibaca dari depan maupun dari belakang
namespace PalindromeChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            //console to ask user to give input
            Console.WriteLine("Masukkan string untuk diperiksa apakah itu palindrom:");
            //input/read input
            string input = Console.ReadLine();

            // call fn (input as argument)
            bool isPalindrome = IsPalindrome(input);

            //print output
            Console.WriteLine("Hasil: " + isPalindrome);
        }

        //the fn to check is the input is palindrome
        static bool IsPalindrome(string input)
        {
            //algo yg di pke adalh sliding window, untuk mengecek palindrom dng membandingkan karakter dari depan dan blkang string secara berurutan
            
            // Hapus spasi dan ubah ke huruf kecil untuk memudahkan perbandingan
            input = input.Replace(" ", "").ToLower();

            int left = 0;
            int right = input.Length - 1;

            while (left < right)
            {
                // Periksa apakah karakter pada indeks left sama dengan karakter pada indeks right
                if (input[left] != input[right])
                {
                    return false; // Bukan palindrom
                }

                // Pindahkan indeks ke kiri dan kanan
                left++;
                right--;
            }

            return true; // Palindrom
        }
    }
}

// tested case 1 : using input word hello with the result output of false on the console
// tested case 2 : using input word katak with the result output of true on the console