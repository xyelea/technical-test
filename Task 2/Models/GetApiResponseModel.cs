﻿namespace Task_2.Models
{
    public class GetApiResponseModel<T>
    {
        public string Message { get; set; }
        public string TransactionId { get; set; }
        public T Data { get; set; }
    }

}
