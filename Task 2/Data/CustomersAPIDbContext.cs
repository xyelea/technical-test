﻿using Microsoft.EntityFrameworkCore;
using Task_2.Models;

namespace Task_2.Data
{
    public class CustomersAPIDbContext : DbContext
    {
        public CustomersAPIDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Customer> Customer { get; set; }
    }
}
