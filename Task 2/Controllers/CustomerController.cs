﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task_2.Data;
using Task_2.Models;

namespace Task_2.Controllers
{

    // solution task 2 menerapkan separation dengan cara memisahkan models based on request
    // di project ini separation cukup dilakukan di models, namun pada saat bootcamp 
    // juga di ajari alternatif cara lain to achieve separation by doing dto ( data transfer object)
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly CustomersAPIDbContext dbContext;

        public CustomerController(CustomersAPIDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        // GetAll
        [HttpGet]
        public async Task<IActionResult> GetCustomers()
        {
            var customers = await dbContext.Customer.ToListAsync();

            var response = new GetApiResponseModel<List<Customer>>()
            {
                Message = "",
                TransactionId = "",
                // dunno how the implementation for response should works
                // if we want to really giving object as response what i know is 
                //var response = new GetApiResponseModel<Customer>()
                //{
                    //Message = "",
                    //TransactionId = "",
                    //Data = customers.FirstOrDefault() // Ambil data pertama dari daftar (array) pelanggan
                //};
            Data = customers // Mengisi daftar dengan semua data pelanggan
            };

            return Ok(response);
        }

        // GetSingleData
        [HttpGet]
        [Route("{id:int}")]
        public async Task<IActionResult> GetCustomer([FromRoute] int id) 
        {
            var costumer = await dbContext.Customer.FindAsync(id);

            if(costumer == null)
            {
                return NotFound();
            }

            return Ok(costumer);
        }
        [HttpPost]
        public async Task<IActionResult> AddCustomer(AddCustomerRequest addCustomerRequest, [FromServices] IMediator mediator)
        {
            var result = await mediator.Send(addCustomerRequest);

            return Ok(result);
        }
        // PUT
        [HttpPut]
        [Route("{id:int}")]
        public async Task<IActionResult> UpdateCustomer([FromRoute] int id, [FromBody] UpdateCustomerRequest updateContactRequest)
        {
            var customer = await dbContext.Customer.FindAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            // Update data pelanggan dengan nilai baru dari updateContactRequest
            customer.CustomerCode = updateContactRequest.CustomerCode;
            customer.CustomerName = updateContactRequest.CustomerName;
            customer.CustomerAddress = updateContactRequest.CustomerAddress;
            customer.ModifiedBy = updateContactRequest.ModifiedBy;
            customer.ModifiedAt = updateContactRequest.ModifiedAt;

            dbContext.Customer.Update(customer);
            await dbContext.SaveChangesAsync();

            return Ok(customer);
        }

        // DELETE
        [HttpDelete]
        [Route("{id:int}")]
        public async Task<IActionResult> DeleteCustomer([FromRoute] int id)
        {
            var customer = await dbContext.Customer.FindAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            dbContext.Customer.Remove(customer);
            await dbContext.SaveChangesAsync();

            return Ok(customer);
        }
    }
}
