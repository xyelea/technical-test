﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Task_2.Data;
using Task_2.Models;

namespace Task_2.CustomerHandler
{
    public class AddCustomerHandler : IRequestHandler<AddCustomerRequest, Customer>
    {
        private readonly CustomersAPIDbContext dbContext;

        public AddCustomerHandler(CustomersAPIDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<Customer> Handle(AddCustomerRequest request, CancellationToken cancellationToken)
        {
            var customer = new Customer
            {
                CustomerCode = request.CustomerCode,
                CustomerName = request.CustomerName,
                CustomerAddress = request.CustomerAddress,
                CreatedBy = request.CreatedBy,
                CreatedAt = request.CreatedAt
            };

            // Simpan data pelanggan ke database
            dbContext.Customer.Add(customer);
            await dbContext.SaveChangesAsync();

            return customer;
        }
    }
}
